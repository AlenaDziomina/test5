<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My first web app</title>
    <script src="http://code.jquery.com/jquery-2.2.4.js" type="text/javascript"></script>
    <script src="app.js" type="text/javascript"></script>
</head>
<body>

<h1>My first web app</h1>
<!--<img src="cat.jpg" alt="My test picture">-->

<br>
<button onclick="location.href='./hello'">Servlet</button>

<br>
<button onclick="location.href='./book'">My book</button>

<br>
<a href="/hello">Servlet2</a>

<br>
<form action="/hello" method="post">
    <input type="text" value="" name="my_name">
    <button type="submit">Submit</button>
</form>

<form action="user.jsp">
    Name: <input type="text" name="username" value="">
    Age: <input type="number" name="userage" value="1">
    <button type="submit">Show user</button>
</form>

<br>
<a href="/config">Config</a>
<p>Session: ${session_id}</p>

<br>
<button onclick="location.href='./library'">My library</button>

Enter the name: <input type="text" id="userName"/>
<h4>Ajax resp:</h4>
<span id="ajaxResponse"></span>

<jsp:include page="WEB-INF/footer.jsp"></jsp:include>
</body>
</html>