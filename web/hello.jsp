<%--
  Created by IntelliJ IDEA.
  User: Александр
  Date: 05.03.2021
  Time: 18:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<h1><c:out value="${message}" default = "Hello world"/></h1>

<c:set var="user" scope="session" value="Ivan"/>
<c:out value="${user}"/>

<c:remove var="user" scope="request"/>
<c:out value="${user}"/>

<c:set var="salary" scope="session" value="${125*6}"/>
<c:if test="${salary > 500}">
    <h2>Salary exeeded limit: <c:out value="${salary}"/> </h2>
</c:if>

<c:set var="age" scope="request" value="26"/>
<c:if test="${age >= 18}">
    <h3>Parents agreement not needed</h3>
</c:if>
<c:if test="${age < 18}">
    <h3>Need Parents agreement</h3>
</c:if>

<c:if test="${age > 18}" scope="request" var="result"/>
<c:out value="${requestScope.result}"/>


<c:set var="a" scope="session" value="${age}"/>
<c:choose>
    <c:when test="${a <=18}">
        a <=18
    </c:when>
    <c:when test="${a > 50}">
        a > 50
    </c:when>
    <c:otherwise>
        not supported
    </c:otherwise>
</c:choose>

</body>
</html>
