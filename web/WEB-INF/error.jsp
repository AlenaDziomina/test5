
<%@ page import="java.util.HashMap" %>
<%@ page import="com.learning.servlet.Calc" %>
<%
    String[] users = new String[]{"Alena", "Alex", "Ivan"};
%>

<%!
    int sqr(int a) {
        return a * a;
    }
%>
<%--
  Created by IntelliJ IDEA.
  User: Александр
  Date: 03.03.2021
  Time: 19:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error page</title>
</head>
<body>

<h1>Error code: <%=response.getStatus()%></h1>
<h2>Message: <%=request.getAttribute("javax.servlet.error.message")%></h2>
<p>2+2 = <%=2+2%></p>
<p>5 > 2 = <%=5 > 2%></p>
<p><%= new String("My" + " app").toUpperCase()%></p>
<p>Today: <%=new java.util.Date()%></p>

<%
    for (int i = 0; i < 5; i++) {
        out.println("<br>Hello " + i);
    }
%>

<%
    for (String user: users) {
        out.println("<br> Username " + user);
    }
%>

<h3>SQR(5) = <%=new Calc().sqr(6)%></h3>

<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>
