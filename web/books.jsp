<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Александр
  Date: 05.03.2021
  Time: 19:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Books in library</title>
</head>
<body>

<c:forEach var="book" items="${books}">
    <c:out value="${book.name} ${book.author} ${book.year}"/>
</c:forEach>

</body>
</html>
