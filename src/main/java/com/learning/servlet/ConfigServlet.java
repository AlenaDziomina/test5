package com.learning.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/config")
public class ConfigServlet extends HttpServlet {

    private static int sessionId = 0;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] config = new String[]{"1.8", "Tomcat"};
        req.setAttribute("app_config", config);

        HttpSession session = req.getSession();
        session.setAttribute("session_id", ++sessionId);

        getServletContext().getRequestDispatcher("/config.jsp").forward(req, resp);
    }
}
