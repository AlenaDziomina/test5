package com.learning.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/library")
public class LibraryServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("book1", "author1", 2001));
        bookList.add(new Book("book2", "author2", 2002));
        bookList.add(new Book("book3", "author3", 2003));

        req.setAttribute("books", bookList);
        getServletContext().getRequestDispatcher("/books.jsp").forward(req, resp);
    }
}
