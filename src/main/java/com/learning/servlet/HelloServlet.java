package com.learning.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloServlet extends HttpServlet {

    private static String myApp;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Set response content type
        resp.setContentType("text/html");

        // Actual logic goes here.
        PrintWriter out = resp.getWriter();
        out.println("<h1>" + myApp + "</h1>");
        out.println("<h2>My first servlet</h2>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String myName = req.getParameter("my_name");
        if (myName.isEmpty()) {
            myName = "Anonim";
        }
        req.setAttribute("message", "Hello " + myName);
        getServletContext().getRequestDispatcher("/hello.jsp").forward(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
        //slkfj;lasjdflk;jadsl;j
    }

    @Override
    public void init() throws ServletException {
        super.init();
        myApp = "My first app";
    }
}
