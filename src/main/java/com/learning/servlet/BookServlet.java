package com.learning.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/book")
public class BookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("my_book", new Book("Java for beg", "unknown", 2021));

        HttpSession session = req.getSession();
        session.setAttribute("my_book", new Book("book2", "author2", 222));
        getServletContext().getRequestDispatcher("/book.jsp").forward(req, resp);
    }
}
