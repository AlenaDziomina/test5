package com.learning.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

@WebServlet("/hello2")
public class HelloAjaxServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName ="Hello " + req.getParameter("userName");

        resp.setContentType("text/plain");
        OutputStream out = resp.getOutputStream();
        out.write(userName.getBytes(StandardCharsets.UTF_8));
        out.flush();
        out.close();
    }
}
